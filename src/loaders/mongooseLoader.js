const mongoose = require('mongoose');

const config = require('../config');

function mongooseLoader() {
    return mongoose.connect(config.db.uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}

module.exports = mongooseLoader;