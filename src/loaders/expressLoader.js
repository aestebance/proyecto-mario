const bodyParser = require('body-parser');
const cors = require('cors');
const router = require('../routes');
const session = require('express-session');

function expressLoader(app) {

    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(session({
        secret: 'twitter',
        resave: false,
        saveUninitialized: false
    }));

    app.use(router);
    app.use(function(req, res) {
        res.sendStatus(404);
    });

    app.use(function(err, req, res){
        res.json({error: err});
    });
}
module.exports = expressLoader;
