const expressLoader = require('./expressLoader');
const mongooseLoader = require('./mongooseLoader');

async function indexLoader(app) {

   // await mongooseLoader();
   //  console.info('Mongoose running');

    expressLoader(app);
    console.info('Express set');
}

module.exports = indexLoader;
