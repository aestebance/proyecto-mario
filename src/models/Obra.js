const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const obraSchema = new mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    date:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    },
    image:{
        type: String,
    },

});

const Obra = mongoose.model('Obra',obraSchema);

module.exports = Obra

