const Obra = require('../models/Obra');

async function createObra(title,date,description,image,id){

    try {
        const newObra = new Obra();
        newObra.title = title;
        newObra.date = date;
        newObra.description = description;
        newObra.image = image;
        newObra.id = id ;
        return await newObra.save();
    } catch (err) {
        throw err;
    }
}

function getObras(filter) {
    return Obra.find(filter);
}

function getObra(filter){
    return Obra.findOne(filter);
}

module.exports = {
    createObra,
    getObra,
    getObras
}