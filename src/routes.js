const express = require('express');
const router = express.Router();
const cloudinaryControllers = require('./controllers/cloudinaryController');
const fileControllers = require('./controllers/fileController');

const obraController = require('./controllers/obraController');

router.route('/images')
    .post(fileControllers.uploadToMemory.single('photo'), cloudinaryControllers.uploadToCloudinary, fileControllers.jsonResponse)
    .get(function (req,res){
        res.json({
            result: 'ok'
        })
    })

module.exports = router
