require('dotenv').config();
const express = require('express');
const loaders = require('./loaders/indexLoader');
const config = require('./config');

serverBootstraping();

function serverBootstraping() {

    const app = express();
    const server = app.listen(config.server.port);

    server.on('error', onError);

    server.on('listening', function() {
        console.info('Server running on port ' + config.server.port);
        loaders(app);
    });
}

function onError(err) {
    switch(err.code) {
        case 'EACCESS':
            console.error(config.server.port + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(config.server.port + ' is already in use');
            process.exit(1);
            break;
        default:
            console.error(err);
            process.exit(1);
    }
}