module.exports = {
    server: {
        port: process.env.PORT || 5500,
        secret: process.env.BACKEND_SECRET || 'backendSecret'
    },
    db: {
        uri: process.env.DB_URI || 'mongodb://localhost:27017/mongoose'
    },
    cloudinary: {
        cloud_name: process.env.CLOUDINARY_NAME,
        api_key: process.env.CLOUDINARY_API_KEY,
        api_secret: process.env.CLOUDINARY_API_SECRET
    },
}
