const crudObra = require('../modules/crudObra');

const addNew = function (req, res){
    const newObra = crudObra.createObra(req.body.title, req.body.date, req.body.description, req.body.image , req.body.id );
    res.json({result:true});
}

const getObras = async function(req,res){
    try{
        res.json(await crudObra.getObras(req.body))
    } catch (err){
        throw err;
    }
}

const getObra = async function(req, res){

    try{
        const Obra = await crudObra.getObra({_id: req.params.id});
        if(Obra){
            res.json(Obra);
        }
        else {
            res.json({result: true});
        }

    }catch (err){
        res.json({result : false});
    }
}

module.exports = {
    addNew,
    getObra,
    getObras
}