const cloudinary = require('cloudinary').v2;
const streamifier = require('streamifier');

const config = require('../config');



function uploadToCloudinary(req,res,done){
    cloudinary.config(config.cloudinary);
    const cloudStream = cloudinary.uploader.upload_stream(
        {
            folder: 'julia-obras'
        },
        function (err,res){
            if(err){
                console.log(err)
                throw err

            }else{
                req.cloudImg = res.url;
                done();
            }
        }
    );
    streamifier.createReadStream(req.file.buffer).pipe(cloudStream);
}

module.exports = {
    uploadToCloudinary,
}

