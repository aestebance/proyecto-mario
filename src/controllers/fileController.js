const multer = require('multer');
const cloudinary = require('./cloudinaryController');

const uploadToMemory = multer({
    storage: multer.memoryStorage()
});

const uploadToCloudinary = async function(req, res, next) {
    try {
        req.image = [];
        for (const file of req.files) {
            const cloudFile = await cloudinary('obras-julia', file.buffer);
            req.image.push({
                name: file.originalname,
                url: cloudFile.url,
            });
        }
        next();
    }
    catch(err) {
        console.log(err);
    }
}

function jsonResponse(req, res) {
    res.json({
        files: req.cloudImg || false,
    });
}

module.exports = {
    uploadToMemory,
    uploadToCloudinary,
    jsonResponse
}
